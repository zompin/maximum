<?php
	/*
		Template name: Франшиза
	*/

	$page_title = 'Франшиза';
	include 'header.php';
?>
<div class="slide slide__franch">
	<div class="slide__box">
		Гарантированная <br> рентабельность!
	</div>
	<div class="slide__text slide__text_franch">
		<div class="slide__text-take">Создай свой бизнес <br> с минимальными рисками</div>
	</div>
	<button class="slide__button slide__button_white order-call">Получить деньги!</button>
</div>
<?php
	get_template_part('inc/interesting');
	get_template_part('inc/ensure');
	get_template_part('inc/for');
	get_template_part('inc/stages');
	get_template_part('inc/choose');
	get_template_part('inc/package');
	get_template_part('inc/phrase');
	include 'footer.php';
?>