<?php

//$base_url = 'm-maximum.kz';
$base_url = 'maximum';

function home_url($path) {
	global $base_url;

	return 'http://' . $base_url . $path;
}

function get_template_directory_uri() {
	return '/';
}

function get_template_part($template) {
	include $template . '.php';
}

function the_title() {
	global $page_title;
	global $base_url;

	if ($page_title) {
		echo $page_title . ' - ' . $base_url;
	} else {
		echo $base_url;
	}
}