<?php
	include 'functions.php';
?><!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?php the_title(); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="/style.css">
</head>
<body>
	<header>
		<div class="header">
			<a href="/" class="header__logo"></a>
			<nav class="header__nav">
				<a href="<?php echo home_url('/zoloto.php'); ?>" class="header__nav-item">Золото</a>
				<a href="<?php echo home_url('/tehnika.php'); ?>" class="header__nav-item">Техника</a>
				<a href="<?php echo home_url('/avto.php'); ?>" class="header__nav-item">Авто</a>
				<a href="<?php echo home_url('/franshiza.php'); ?>" class="header__nav-item">Франшиза</a>
			</nav>
			<div class="header__contacts">
				<a href="tel:+77759762051" class="header__contacts-link header__contacts-phone">+7 /775/ 976 20 51</a>
				<a href="https://www.instagram.com/maximumlombard/" class="header__contacts-link header__contacts-inst" target="_blank">@maximumlombard</a>
			</div>
		</div>
		<nav class="adaptive-nav">
			<a href="<?php echo home_url('/zoloto.php'); ?>" class="adaptive-nav__item">Золото</a>
			<a href="<?php echo home_url('/tehnika.php'); ?>" class="adaptive-nav__item">Техника</a>
			<a href="<?php echo home_url('/avto.php'); ?>" class="adaptive-nav__item">Авто</a>
			<a href="<?php echo home_url('/franshiza.php'); ?>" class="adaptive-nav__item">Франшиза</a>
			<button href="#" class="adaptive-nav__item order-call">Позвонить нам</button>
			<a href="https://www.instagram.com/maximumlombard/" class="adaptive-nav__item" target="_blank">Мы в Instagram</a>
		</nav>
		<button class="adaptive-nav-button"></button>
	</header>