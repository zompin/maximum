<?php
	/*
		Template name: Золото
	*/
	
	$page_title = 'Золото';
	include 'header.php';
?>
<div class="slide slide__gold">
	<div class="slide__box">
		Быстрые займы
		<br>
		Ювелирных изделий
	</div>
	<div class="slide__text">
		<div class="slide__text-take">Возьми деньги</div>
		<div class="slide__text-without">Без процентов!</div>
	</div>
	<button class="slide__button order-call">Получить деньги!</button>
</div>
<?php
	get_template_part('inc/wares-gold');
	get_template_part('inc/advantages-gold');
	get_template_part('inc/need-gold');
	get_template_part('inc/scheme-gold');
	get_template_part('inc/feedback-gold');
	include 'footer.php';
?>