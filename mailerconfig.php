<?php
	include 'emails.php';
	$mail = new PHPMailer;

	//$mail->SMTPDebug 	= 4;
	$mail->isSMTP();
	$mail->CharSet 		= 'utf-8';
	$mail->Timeout 		= 120;
	$mail->Host 		= 'r-finance.kz';
	$mail->SMTPAuth 	= true;
	$mail->Username 	= $email;
	$mail->Password 	= $pass;
	$mail->SMTPSecure 	= 'starttls';
	$mail->Port 		= 25;
	$mail->setFrom($email);
	$mail->isHTML(true);
	$mail->SMTPOptions = array(
		'ssl' => array(
			'verify_peer' 		=> false,
			'verify_peer_name' 	=> false,
			'allow_self_signed' => true
		)
	);
?>