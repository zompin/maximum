	<?php
		get_template_part('inc/call');
		get_template_part('inc/contacts');
		get_template_part('inc/map');
	?>
	<footer>
		<div class="footer">
			<a href="/" class="footer__logo"></a>
			<nav class="footer__menu">
				<a href="<?php echo home_url('/zoloto.php'); ?>" class="footer__menu-item">Золото</a>
				<a href="<?php echo home_url('/tehnika.php'); ?>" class="footer__menu-item">Техника</a>
				<a href="<?php echo home_url('/avto.php'); ?>" class="footer__menu-item">Авто</a>
				<a href="<?php echo home_url('/franshiza.php'); ?>" class="footer__menu-item">Франшиза</a>
			</nav>
			<button class="button footer__get-money order-call">Получить деньги</button>
		</div>
	</footer>
	<button class="to-top"></button>
	<?php
		get_template_part('inc/callback-form');
		get_template_part('inc/feedback-form');
	?>
	<script src="<?php echo get_template_directory_uri() . 'js/jquery/dist/jquery.min.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . 'js/slick-carousel/slick/slick.min.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . 'js/main.js'; ?>"></script>
	<script src="<?php echo get_template_directory_uri() . 'js/map-delay.js'; ?>"></script>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter44662849 = new Ya.Metrika({
	                    id:44662849,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true,
	                    webvisor:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/watch.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/44662849" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	<!--
		Алтухов Илья
		zompin@ya.ru
	-->
</body>
</html>