;(function() {
	var ymapi 	= document.createElement('script');
	var map 	= document.createElement('script');

	ymapi.src 	= 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
	map.src 	= '/js/map.js';
	ymapi.onload 	= function() {
		document.head.appendChild(map);
	}

	setTimeout(function() {
		document.head.appendChild(ymapi);
	}, 2000);
})();