;(function($) {

	// Слайдеры
	$('.slider__items').slick({
		autoplay: true,
		arrows: true,
		nextArrow: '.slider__control-button_next',
		prevArrow: '.slider__control-button_prev',
		autoplaySpeed: 3000,
		dots: true
	});

	$('.package__items').slick({
		slidesToShow: 3,
  		slidesToScroll: 1,
  		arrows: true,
  		nextArrow: '.package__control-button_next',
  		prevArrow: '.package__control-button_prev',
  		responsive: [
  			{
  				breakpoint: 1024,
  				settings: {
  					slidesToShow: 2,
  					dots: true
  				}
  			}, {
  				breakpoint: 780,
  				settings: {
  					slidesToShow: 1,
  					dots: true
  				}
  			}
  		]
	});

	$('.feedback__items').slick({
		arrows: true,
		nextArrow: '.feedback__button_next',
		prevArrow: '.feedback__button_prev'
	});

	// Кнопка вверх
	$('.to-top').click(function() {
		$('html, body').stop().animate({scrollTop: 0});
	});

	// Формы, показать скрыть
	$('.order-call').click(function() {
		$('.form__cont_callback').show();
	});

	$('.form__close, .form__close-div').click(function() {
		$('.form__cont_callback').hide();
	});

	$('.feedback__send').click(function() {
		$('.form__cont_feedback').show();
	});

	$('.form__close, .form__close-div').click(function() {
		$('.form__cont_feedback').hide();
	});

	// Кнопка адптивного меню
	$('.adaptive-nav-button').click(function() {
		$('.adaptive-nav').toggleClass('adaptive-nav_showed');
		$('.adaptive-nav-button').toggleClass('adaptive-nav-button_active');
	});

	$('.call__button').click(function() {
		var data 	= {};
		var url 		= '/sendcallback.php';
		data.name 	= $('.call__input_name').val();
		data.phone 	= $('.call__input_phone').val();
		var messages = {
			success: 'Звонок заказан',
			error: 'Не удалось заказать звонок'
		};

		if (!data.name) {
			alert('Вы не ввели имя');
			return;
		}

		if (!data.phone) {
			alert('Вы не ввели телефон');
			return;
		}

		sendReq(data, messages, url);
	});

	$('.form__button_callback').click(function() {
		var data 		= {};
		var url 		= '/sendcallback.php';
		data.name 		= $('.form__input_name-callback').val();
		data.phone 		= $('.form__input_phone-callback').val();
		data.timeStart 	= $('.form__select_start').val();
		data.timeEnd 	= $('.form__select_end').val();
		var messages 	= {
			success: 'Звонок заказан',
			error: 'Не удалось заказать звонок'
		};

		if (!data.name) {
			alert('Вы не ввели имя');
			return;
		}

		if (!data.phone) {
			alert('Вы не ввели телефон');
			return;
		}

		sendReq(data, messages, url);
	});

	$('.form__button_feedback').click(function() {
		var data 		= {};
		var url 		= '/sendfeedback.php';
		data.name 		= $('.form__input_name-feedback').val();
		data.city 		= $('.form__input_city-feedback').val();
		data.email 		= $('.form__input_email-feedback').val();
		data.message 	= $('.form__textarea').val();
		data.captcha 	= $('.form__input_captcha').val();
		var messages 	= {
			success: 'Отзыв отправлен',
			error: 'Не удалось отправить отзыв'
		};

		if (!data.name) {
			alert('Вы не ввели имя');
			return;
		}

		if (!data.city) {
			alert('Вы не ввели город');
			return;
		}

		if (!data.email) {
			alert('Вы не ввели e-mail');
			return;
		}

		if (!data.captcha) {
			alert('Вы не ввели цифры с картинки');
			return;
		}

		sendReq(data, messages, url);
	});

	function sendReq(data, messages, url) {
		$('.form__button, .call__button').prop('disabled', true);
		$.ajax({
			method: 'POST',
			url: url,
			data: data,
			success: function(data) {

				if (data == '1') {
					alert(messages.success);
				} else {
					if (data == '0') {
						alert(messages.error);
					} else {
						alert(data);
					}
				}

				$('.form__button, .call__button').prop('disabled', false);
			},
			error: function() {
				alert(messages.error);
				$('.form__button, .call__button').prop('disabled', false);
			}
		});
	}

	$('.form__select').append(function() {
		var options = '<option value="0"></option>';

		for (var i = 1; i < 25; i++) {
			options += '<option value="' + i + ':00">' + i + ':00</option>';
		}

		return options;
	});

	$('.header__nav-item').each(function(i, e) {
		var path = location.pathname.replace(/\//ig, '');
		var href = $(e).attr('href');

		if (path.length && href.indexOf(path) >= 0) {
			$(e).addClass('header__nav-item_current');
		}
	});

	$('.adaptive-nav__item').each(function(i, e) {
		var path = location.pathname.replace(/\//ig, '');
		var href = $(e).attr('href');


		if (path.length && href.indexOf(path) >= 0) {
			$(e).addClass('adaptive-nav__item_current');
		}
	});
})(jQuery);