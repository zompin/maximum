var map = document.getElementById('map');

if (map) {
	var myMap;
	ymaps.ready(function() {
		myMap = new ymaps.Map(map, {
			//center: [51.161619, 71.457863],
			center: [51.1619, 71.4570],
			zoom: 18,
			controls: []
		});
		var address = document.querySelector('.contacts__item_place').firstChild.data;
		var template = ymaps.templateLayoutFactory.createClass('<div id="map__mark">' + address + '</div>');
		var placeMark = new ymaps.Placemark(
				myMap.getCenter(), {}, {
					iconLayout: template
				}
			);

		if (address) {
			myMap.geoObjects.add(placeMark);
		}
		
		myMap.behaviors.disable("scrollZoom");
	});
}