<section class="feedback">
	<h1 class="feedback__header">Отзывы клиентов</h1>
	<div class="feedback__control">
		<button class="feedback__button feedback__button_prev"></button>
		<button class="feedback__button feedback__button_next"></button>
	</div>
	<div class="feedback__items">
		<div class="feedback__item">
			<div class="feedback__text">В феврале очень срочно нужны были деньги, поэтому решил воспользоваться услугами автоломбарда «МАКСИМУМ». Друг сказал, что сотрудники сработают оперативно. Как говорится: «Пришел, увидел, получил». Быстренько оценили машину, попросили пару документов и в течении часа выдали нужную сумму. Спасибо за оперативность!</div>
			<div class="feedback__author">Алексей</div>
			<div class="feedback__city">Астана</div>
		</div>
	</div>
	<button class="feedback__send">Прислать отзыв</button>
</section>