<section class="scheme">
	<h1 class="scheme__header">Схема получения займа</h1>
	<div class="scheme__items">
		<div class="scheme__item scheme__item_rate-tech">
			<div class="scheme__item-header">
				<p class="scheme__p">Оценка вашей</p>
				<p class="scheme__p">техники</p>
			</div>
		</div>
		<div class="scheme__item scheme__item_arrow"></div>
		<div class="scheme__item scheme__item_hands">
			<div class="scheme__item-header">
				<p class="scheme__p">Согласование суммы</p>
				<p class="scheme__p">и срока займа</p>
			</div>
		</div>
		<div class="scheme__item scheme__item_arrow"></div>
		<div class="scheme__item scheme__item_contract">
			<div class="scheme__item-header">
				<p class="scheme__p">Заключение договора</p>
				<p class="scheme__p">и страхование</p>
				<p class="scheme__p">оставляемого залога</p>
			</div>
		</div>
		<div class="scheme__item scheme__item_arrow"></div>
		<div class="scheme__item scheme__item_wallet">
			<div class="scheme__item-header">
				<p class="scheme__p">Получение необходимой</p>
				<p class="scheme__p">вам суммы</p>
			</div>
		</div>
	</div>
</section>