<section class="feedback">
	<h1 class="feedback__header">Отзывы клиентов</h1>
	<div class="feedback__control">
		<button class="feedback__button feedback__button_prev"></button>
		<button class="feedback__button feedback__button_next"></button>
	</div>
	<div class="feedback__items">
		<div class="feedback__item">
			<div class="feedback__text">Ломбард Максимум помог мне справиться с небольшими финансовыми трудностями. Сдала в ломбард золото и кое-что из техники. Порадовал сервис, сотрудники очень приветливые, быстро оценили золото, технику и назвали цену, которая кстати говоря, вышла даже больше, чем я ожидала. Спасибо вам большое! </div>
			<div class="feedback__author">Арай</div>
			<div class="feedback__city">Астана</div>
		</div>
	</div>
	<button class="feedback__send">Прислать отзыв</button>
</section>