<section class="package">
	<h1 class="package__header">Выберите свой партнёрский пакет</h1>
	<div class="package__control">
		<button class="package__control-button package__control-button_prev"></button>
		<button class="package__control-button package__control-button_next"></button>
	</div>
	<div class="package__items">
		<div class="package__item">
			<div class="package__item-inner">
				<div class="package__item-header">Партнёр</div>
				<div class="package__item-price">0 <span class="currency">b</span></div>
				<ul class="package__list">
					<li class="package__list-item">Позвоните, и мы расскажем вам о нашем предложении.</li>
				</ul>
				<button class="package__button order-call">Узнать больше</button>
			</div>
		</div>
		<div class="package__item">
			<div class="package__item-inner">
				<div class="package__item-header">Консалтинг</div>
				<div class="package__item-price">500 000 <span class="currency">b</span></div>
				<ul class="package__list">
					<li class="package__list-item">Помощь в открытии ломбарда от А до Я</li>
					<li class="package__list-item">Без бренда и роялти</li>
				</ul>
				<button class="package__button order-call">Узнать больше</button>
			</div>
		</div>
		<div class="package__item">
			<div class="package__item-inner">
				<div class="package__item-header">Стандарт</div>
				<div class="package__item-price">1 000 000 <span class="currency">b</span></div>
				<ul class="package__list">
					<li class="package__list-item">Фирменный стиль</li>
					<li class="package__list-item">Юридическая поддержка</li>
					<li class="package__list-item">Бухгалтерская поддержка</li>
					<li class="package__list-item">Обучение эксперт-оценщика</li>
					<li class="package__list-item">Консультации на всех этапах работы</li>
					<li class="package__list-item">Инструкции по реализации имущества</li>
					<li class="package__list-item">Маркетинговые материалы</li>
					<li class="package__list-item">Стратегия развития</li>
					<li class="package__list-item">Рекомендации по работе ОПС И ТС</li>
					<li class="package__list-item">Финансовая модель</li>
					<li class="package__list-item">Управленческие материалы</li>
				</ul>
				<button class="package__button order-call">Узнать больше</button>
			</div>
		</div>
		<div class="package__item">
			<div class="package__item-inner">
				<div class="package__item-header">Под ключ</div>
				<div class="package__item-price">2 000 000 <span class="currency">b</span></div>
				<ul class="package__list">
					<li class="package__list-item">Фирменный стиль</li>
					<li class="package__list-item">Юридическая поддержка</li>
					<li class="package__list-item">Бухгалтерская поддержка</li>
					<li class="package__list-item">Набор и обучение сотрудников</li>
					<li class="package__list-item">Консультации на всех этапах работы</li>
					<li class="package__list-item">Инструкции по реализации имущества</li>
					<li class="package__list-item">Маркетинговые материалы</li>
					<li class="package__list-item">Поиск помещения</li>
					<li class="package__list-item">Инструкция руководителю Ломбарда</li>
					<li class="package__list-item">Открытие ломбарда «Под ключ» нашими специалистами</li>
					<li class="package__list-item">Финансовая модель</li>
					<li class="package__list-item">Управленческие материалы</li>
				</ul>
				<div class="package__item-text">
					<span class="package__star">*</span>В данном пакете всю работу делают наши специалисты по франчайзингу.
				</div>
				<button class="package__button order-call">Узнать больше</button>
			</div>
		</div>
		<div class="package__item">
			<div class="package__item-inner">
				<div class="package__item-header">Под ключ vip</div>
				<div class="package__item-price">3 000 000 <span class="currency">b</span></div>
				<ul class="package__list">
					<li class="package__list-item">Фирменный стиль</li>
					<li class="package__list-item">Юридическая поддержка</li>
					<li class="package__list-item">Бухгалтерская поддержка</li>
					<li class="package__list-item">Набор и обучение сотрудников</li>
					<li class="package__list-item">Консультации на всех этапах работы</li>
					<li class="package__list-item">Инструкции по реализации имущества</li>
					<li class="package__list-item">Маркетинговые материалы</li>
					<li class="package__list-item">Поиск помещения</li>
					<li class="package__list-item">Инструкция руководителю Ломбарда</li>
					<li class="package__list-item">Открытие ломбарда «Под ключ» нашими специалистами</li>
					<li class="package__list-item">Все расходы на открытие берем на себя</li>
				</ul>
				<div class="package__item-text">
					<span class="package__star">*</span>Выбрав этот пакет вы получаете готовый ломбард в любом городе РК!
				</div>
				<button class="package__button order-call">Узнать больше</button>
			</div>
		</div>
	</div>
	<div class="package__text">
		* выезд специалистов в другой город Республики Казахстан кроме г. Астаны
		<div class="package__text_bold">+ 300 000 тенге</div>
	</div>
</section>