<section class="contacts">
	<h1 class="contacts__header">Ломбард «максимум»</h1>
	<div class="contacts__items">
		<div class="contacts__item contacts__item_place">Астана, ул. Иманова д. 41 ЖК Жар-Жар</div>
		<a class="contacts__item contacts__item_link contacts__item_phone" href="tel:+77759762051">+7 /775/ 976 20 51</a>
		<a class="contacts__item contacts__item_link contacts__item_inst" href="https://www.instagram.com/maximumlombard/">@maximumlombard</a>
		<div class="contacts__item contacts__item_schedule">10.00 - 20.00</div>
	</div>
</section>