<section class="wares">
	<h1 class="wares__header">Какую технику мы принимаем?</h1>
	<div class="wares__items">
		<div class="wares__item wares__item_phones">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">от 0,6%</p>
				<p class="wares__p">в сутки!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Мобильные телефоны</div>
			</div>
		</div>
		<div class="wares__item wares__item_notebooks">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">быстрые займы</p>
				<p class="wares__p">до 100 000 тг</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Ноутбуки и планшеты</div>
			</div>
		</div>
		<div class="wares__item wares__item_apple">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">самая высокая</p>
				<p class="wares__p">оценка!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Техника apple</div>
			</div>
		</div>
		<div class="wares__item wares__item_playstations">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">получи деньги</p>
				<p class="wares__p">и время!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Игровые приставки</div>
			</div>
		</div>
		<div class="wares__item wares__item_tv">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">лучшие новости -</p>
				<p class="wares__p">деньги в кармане!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Телевизоры</div>
			</div>
		</div>
		<div class="wares__item wares__item_other">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">не пользуешься -</p>
				<p class="wares__p">сдай в ломбард!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__type">Другие изделия</div>
			</div>
		</div>
	</div>
</section>