<section class="for">
	<h1 class="for__header">Для чего вам франшиза?</h1>
	<div class="for__text">Статистика говорит о том, что...</div>
	<div class="for__items">
		<div class="for__item for__item_70">
			<div class="for__item-percent for__item-percent_70">70%</div>
			<div class="for__item-header for__item-header_70">ПРЕДПРИНИМАТЕЛЕЙ</div>
			<div class="for__item-desc for__item-desc_70">
				<p class="for__p">закрывают бизнес </p>
				<p class="for__p">в течение 1-го года.</p>
			</div>
			<div class="for__image for__image_70"></div>
		</div>
		<div class="for__item for__item_50">
			<div class="for__image for__image_50"></div>
			<div class="for__item-percent for__item-percent_50">50%</div>
			<div class="for__item-header for__item-header_50">«ВЫЖИВШИХ»</div>
			<div class="for__item-desc for__item-desc_50">
				<div class="for__p">закрываются в течение</div>
				<div class="for__p">2-го года работы</div>
			</div>
		</div>
		<div class="for__item for__item_90">
			<div class="for__item-percent for__item-percent_90">90%</div>
			<div class="for__item-header for__item-header_90">ФРАНЧАЙЗИ</div>
			<div class="for__item-desc for__item-desc_90">
				<p class="for__p">продолжают успешно работать</p>
				<p class="for__p">и через 2 года!</p>
			</div>
			<div class="for__image for__image_90"></div>
		</div>
	</div>
	<div class="for__text">
		<p class="for__p">Таким образом, франшиза на порядок снижает</p>
		<p class="for__p">риски убытков и последующего закрытия бизнеса.</p>
	</div>
</section>