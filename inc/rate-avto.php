<section class="rate">
	<div class="rate__item rate__item_image rate__item_image-avto"></div>
	<div class="rate__item">
		<h1 class="rate__header">Как мы оцениваем автомобиль?</h1>
		<div class="rate__list">
			<div class="rate__list-item rate__list-item_one">Тип автомобиля</div>
			<div class="rate__list-item rate__list-item_two">Год выпуска</div>
			<div class="rate__list-item rate__list-item_three">
				Техническое состояние
				<div class="rate__list-item-desc">(работоспособность, необходимость в ремонте)</div>
			</div>
			<div class="rate__list-item rate__list-item_four">
				Внешний вид
				<div class="rate__list-item-desc">(наличие царапин, трещин)</div>
			</div>
		</div>
	</div>
</section>