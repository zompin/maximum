<section class="why">
	<h1 class="why__header">Почему клиенты выбирают нас?</h1>
	<div class="why__items">
		<div class="why__item why__item_percent">
			<h2 class="why__item-header">Низкие проценты</h2>
			<div class="why__item-text">
				<p class="why__p">Мы предоставляем нашим клиентам</p>
				<p class="why__p">самые низкие процентные ставки</p>
				<p class="why__p">в Астане - от 0,4% в день!</p>
			</div>
		</div>
		<div class="why__item why__item_raiting">
			<h2 class="why__item-header">Высокая оценка золота</h2>
			<div class="why__item-text">
				<p class="why__p">Мы оцениваем залоговое имущество</p>
				<p class="why__p">от 50 до 80% от рыночной стоимости</p>
				<p class="why__p">в зависимости от частоты обращений.</p>
			</div>
		</div>
		<div class="why__item why__item_docs">
			<h2 class="why__item-header">Минимум документов</h2>
			<div class="why__item-text">
				<p class="why__p">Для получения займа вам понадобиться</p>
				<p class="why__p">только залоговое имущество</p>
				<p class="why__p">и удостоверение личности</p>
			</div>
		</div>
		<div class="why__item why__item_reg">
			<h2 class="why__item-header">Быстрое оформление</h2>
			<div class="why__item-text">
				<p class="why__p">Мы оформляем кредит за 15 минут!</p>
				<p class="why__p">Никиких ожиданий, согласований</p>
				<p class="why__p">и дополнительной информации.</p>
			</div>
		</div>
		<div class="why__item why__item_loyalti">
			<h2 class="why__item-header">Продвинутая программа лояльности</h2>
			<div class="why__item-text">
				<p class="why__p">Все новые клиенты получают бесплатный </p>
				<p class="why__p">кредит на первые 5 дней, а постоянные -</p>
				<p class="why__p">повышенную оценку и низкие проценты!</p>
			</div>
		</div>
		<div class="why__item why__item_attitude">
			<h2 class="why__item-header">Внимательное отношение</h2>
			<div class="why__item-text">
				<p class="why__p">Мы помогаем нашим клиентам</p>
				<p class="why__p">в трудных  ситуациях и дорожим</p>
				<p class="why__p">нашим сотрудничеством.</p>
			</div>
		</div>
	</div>
</section>