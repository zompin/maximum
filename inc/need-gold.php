<section class="need">
	<div class="need__item need__item_background need__item_background-money"></div>
	<div class="need__item need__item_desc">
		<h1 class="need__header">Что нужно, чтобы получить деньги?</h1>
		<div class="need__list">
			<div class="need__list-item need__list-item_18">Быть старше 18 лет!</div>
			<div class="need__list-item need__list-item_id">Ваше удостоверение личности.</div>
			<div class="need__list-item need__list-item_jevel">Ваше ювелирное изделие.</div>
			<div class="need__list-item need__list-item_visit">Посетить ломбард «Максимум».</div>
		</div>
	</div>
</section>