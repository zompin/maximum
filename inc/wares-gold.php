<section class="wares">
	<h1 class="wares__header">Какие изделия принимаются?</h1>
	<div class="wares__items">
		<div class="wares__item wares__item_jevels">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">от 0,4%</p>
				<p class="wares__p">в сутки!</p>
			</div>
			<div class="wares__desc">
				<div class="wares__money">Деньги под залог</div>
				<div class="wares__type">Ювелирных изделий</div>
			</div>
		</div>
		<div class="wares__item wares__item_lom">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">от 10000 тенге</p>
				<p class="wares__p">за грамм</p>
			</div>
			<div class="wares__desc">
				<div class="wares__money">Деньги под залог</div>
				<div class="wares__type">Золотого лома</div>
			</div>
		</div>
		<div class="wares__item wares__item_metals">
			<div class="wares__item-images"></div>
			<div class="wares__text">
				<p class="wares__p">экспертная</p>
				<p class="wares__p">оценка</p>
			</div>
			<div class="wares__desc">
				<div class="wares__money">Деньги под залог</div>
				<div class="wares__type">Изделий из драгметаллов</div>
			</div>
		</div>
	</div>
</section>