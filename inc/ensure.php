<section class="ensure">
	<h1 class="ensure__header">В этом бизнесе мы гарантируем вам:</h1>
	<div class="ensure__items">
		<div class="ensure__item ensure__item_1">От 1 млн. тг для открытия (паушальный взнос + деньги на запуск)</div>
		<div class="ensure__item ensure__item_30">Всего 30 дней на полноценный запуск прибыльного бизнеса</div>
		<div class="ensure__item ensure__item_16-32">Ежемесячный прирост денежных средств от 16 до 32%</div>
		<div class="ensure__item ensure__item_4">Полный срок окупаемости расходов на открытие</div>
	</div>
</section>