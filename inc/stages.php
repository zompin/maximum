<section class="stages">
	<h1 class="stages__header">Как зарабатывать вместе с ломбардом «максимум»?</h1>
	<div class="stages__items">
		<div class="stages__item stages__item_number stages__item_one">
			<p class="stages__p">Становитесь партнёром</p>
			<p class="stages__p">ломбарда «МАКСИМУМ»</p>
		</div>
		<div class="stages__item stages__item_arrow"></div>
		<div class="stages__item stages__item_number stages__item_two">
			<p class="stages__p">Находим перспективную</p>
			<p class="stages__p">аренду под ваш ломбард</p>
		</div>
		<div class="stages__item stages__item_arrow"></div>
		<div class="stages__item stages__item_number stages__item_three">
			<p class="stages__p">Выдаёте займы</p>
			<p class="stages__p">под проценты (от 16 до 32%)</p>
			<p class="stages__p">и увеличиваете прибыль</p>
		</div>
		<div class="stages__item stages__item_arrow"></div>
		<div class="stages__item stages__item_number stages__item_four">
			<p class="stages__p">Наращиваете оборотные</p>
			<p class="stages__p">средства и расширяете</p>
			<p class="stages__p">сеть ломбардов</p>
		</div>
	</div>
</section>