<section class="call">
	<h1 class="call__header">Нужны деньги?</h1>
	<div class="call__text">Оставьте заявку, и мы перезвоним вам в ближайшее время</div>
	<div class="call__form">
		<input type="text" class="call__input call__input_name" placeholder="Имя">
		<input type="text" class="call__input call__input_phone"  placeholder="Телефон">
		<button class="call__button">Заказать звонок</button>
	</div>
</section>