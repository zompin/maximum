<div class="slider">
	<div class="slider__control">
		<button class="slider__control-button slider__control-button_prev"></button>
		<button class="slider__control-button slider__control-button_next"></button>
	</div>
	<div class="slider__items">
		<div class="slider__item">
			<div class="slider__item-image slider__item-image_money"></div>
			<div class="slider__header">Возьми деньги <br> без процентов!</div>
			<div class="slider__text slider__text_money">Максимально лёгкие займы!</div>
			<button class="slider__button order-call">Получить деньги!</button>
		</div>
		<div class="slider__item">
			<a href="<?php echo home_url('/zoloto.php'); ?>" class="slider__item-image slider__item-image_gold"></a>
			<div class="slider__header">Срочно <br> нужны деньги?</div>
			<div class="slider__text">Поможем максимально быстро!</div>
			<div class="slider__box">Займы под залог <br> ювелирных изделий</div>
			<button class="slider__button order-call">Получить деньги!</button>
		</div>
		<div class="slider__item">
			<a href="<?php echo home_url('/tehnika.php'); ?>" class="slider__item-image slider__item-image_tech"></a>
			<div class="slider__header">Срочно <br> нужны деньги?</div>
			<div class="slider__text">Поможем максимально быстро!</div>
			<div class="slider__box">Займы под залог <br> техники</div>
			<button class="slider__button order-call">Получить деньги!</button>
		</div>
		<div class="slider__item">
			<a href="<?php echo home_url('/avto.php'); ?>" class="slider__item-image slider__item-image_car"></a>
			<div class="slider__header">Срочно <br> нужны деньги?</div>
			<div class="slider__text">Поможем максимально быстро!</div>
			<div class="slider__box">Займы под залог <br> автомобиля</div>
			<button class="slider__button order-call">Получить деньги!</button>
		</div>
		<div class="slider__item">
			<a href="<?php echo home_url('/franshiza.php'); ?>" class="slider__item-image slider__item-image_franch"></a>
			<div class="slider__header slider__header_franch">зарабатывайте <br> вместе с нами!</div>
			<div class="slider__text slider__text_franch">Мы знаем, как достичь <br> рентабельности до 300% в год <br> готовы этим делиться.</div>
			<button class="slider__button slider__button_white order-call">Получить деньги!</button>
		</div>
	</div>
</div>