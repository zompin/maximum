<section class="discounts">
	<h1 class="discounts__header">Постоянным клиентам - выгодные условия!</h1>
	<div class="discounts__items">
		<div class="discounts__item discounts__item_new">
			<h2 class="discounts__item-header">Первые 5 дней - бесплатно!</h2>
			<div class="discounts__item-text">
				<p class="discounts__p">Золото - 0,3% в день.</p>
				<p class="discounts__p">Техника - 0,4% в день.</p>
				<p class="discounts__p">Оценка залога - 50%</p>
				<p class="discounts__p">от рыночной стоимости.</p>
			</div>
			<div class="discounts__item-time">Для новых клиентов</div>
		</div>
		<div class="discounts__item discounts__item_active">
			<h2 class="discounts__item-header">Оценка становится выше!</h2>
			<div class="discounts__item-text">
				<p class="discounts__p">Золото - 0,3% в день.</p>
				<p class="discounts__p">Техника - 0,4% в день.</p>
				<p class="discounts__p">Оценка залога - 60%</p>
				<p class="discounts__p">от рыночной стоимости.</p>
			</div>
			<div class="discounts__item-time">Со 2 по 5 обращение</div>
		</div>
		<div class="discounts__item discounts__item_vip">
			<h2 class="discounts__item-header">Досрочное погашение с перерасчетом дней!</h2>
			<div class="discounts__item-text">
				<p class="discounts__p">Золото - 0,3% в день.</p>
				<p class="discounts__p">Техника - 0,4% в день.</p>
				<p class="discounts__p">Оценка залога - 70%</p>
				<p class="discounts__p">от рыночной стоимости.</p>
			</div>
			<div class="discounts__item-time">С 6 по 10 обращение</div>
		</div>
		<div class="discounts__item discounts__item_friends">
			<h2 class="discounts__item-header">Погашайте кредит по частям!</h2>
			<div class="discounts__item-text">
				<p class="discounts__p">Золото - 0,3% в день.</p>
				<p class="discounts__p">Техника - 0,4% в день.</p>
				<p class="discounts__p">Оценка залога - 80%</p>
				<p class="discounts__p">от рыночной стоимости.</p>
			</div>
			<div class="discounts__item-time">После 10 обращения</div>
		</div>
	</div>
	<button class="discounts__button order-call">Узнать больше</button>
</section>