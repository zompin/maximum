<section class="advantages">
	<h1 class="advantages__header">Преимущества получения кредита под технику</h1>
	<div class="advantages__items">
		<div class="advantages__item advantages__item_reg">
			<p class="advantages__p">Быстрое оформление</p>
			<p class="advantages__p">кредита</p>
		</div>
		<div class="advantages__item advantages__item_safe">
			<p class="advantages__p">Гарантия сохранности</p>
			<p class="advantages__p">залога</p>
		</div>
		<div class="advantages__item advantages__item_percent">
			<p class="advantages__p">Минимальные </p>
			<p class="advantages__p">процентные ставки</p>
		</div>
		<div class="advantages__item advantages__item_pay">
			<p class="advantages__p">Мгновенная</p>
			<p class="advantages__p">оплата</p>
		</div>
	</div>
	<button class="advantages__button order-call">Получить деньги!</button>
</section>