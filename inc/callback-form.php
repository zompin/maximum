<div class="form__cont form__cont_callback">
	<div class="form form_callback">
		<div class="form__close-div"></div>
		<div class="form__inner form__inner_callback">
			<button class="form__close"></button>
			<h1 class="form__header">Здравствуйте</h1>
			<div class="form__text">Оставьте номер, и мы Вам перезвоним!</div>
			<label class="form__label form__label_w">
				<span class="form__label-caption">Имя*</span>
				<input type="text" class="form__input form__input_name form__input_name-callback">
			</label>
			<label class="form__label form__label_w">
				<span class="form__label-caption">Мобильный телефон*</span>
				<input type="text" class="form__input form__input_phone form__input_phone-callback">
			</label>
			<!--<div class="form__text form__text_time">Удобное Вам время звонка:</div>
			<div class="form__time">
				<span class="form__preselect form__preselect_from">С</span>
				<select class="form__select form__select_start"></select>
				<span class="form__preselect form__preselect_to">До</span>
				<select class="form__select form__select_end"></select>
			</div>-->
			<button class="form__button form__button_callback">Позвоните мне!</button>
		</div>
	</div>
</div>