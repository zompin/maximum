<section class="scheme">
	<h1 class="scheme__header">КАК МЫ РАБОТАЕМ?</h1>
	<div class="scheme__items">
		<div class="scheme__item scheme__item_w scheme__item_rate-gold">
			<div class="scheme__item-header">Оцениваем Ваш залог</div>
			<div class="scheme__item-text">
				<p class="scheme__p">Мы оцениваем залоговое имущество</p>
				<p class="scheme__p">от 50 до 80% от рыночной стоимости</p>
				<p class="scheme__p">в зависимости от частоты ваших обращений.</p>
			</div>
		</div>
		<div class="scheme__item scheme__item_arrow"></div>
		<div class="scheme__item scheme__item_w scheme__item_contract">
			<div class="scheme__item-header">Заключаем договор</div>
			<div class="scheme__item-text">
				<p class="scheme__p">Для заключения договора займа</p>
				<p class="scheme__p">вам нужен только документ</p>
				<p class="scheme__p">удостоверяющий личность!</p>
			</div>
		</div>
		<div class="scheme__item scheme__item_arrow"></div>
		<div class="scheme__item scheme__item_w scheme__item_wallet">
			<div class="scheme__item-header">Выдаём деньги!</div>
			<div class="scheme__item-text">
				<p class="scheme__p">Вы получаете необходимые вам средства</p>
				<p class="scheme__p">на срок от 10 до 90 дней по низкой ставке</p>
				<p class="scheme__p">и без проволочек.</p>
			</div>
		</div>
	</div>
</section>