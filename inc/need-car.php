<section class="need">
	<div class="need__item need__item_background need__item_background-car"></div>
	<div class="need__item need__item_desc">
		<h1 class="need__header">Что нужно, чтобы получить деньги?</h1>
		<div class="need__list">
			<div class="need__list-item need__list-item_18">Быть старше 18 лет!</div>
			<div class="need__list-item need__list-item_id">Ваше удостоверение личности</div>
			<div class="need__list-item need__list-item_car">Ваш автомобиль</div>
			<div class="need__list-item need__list-item_passport">Технический паспорт автомобиля</div>
			<div class="need__list-item need__list-item_ensurance">Страховка</div>
			<div class="need__list-item need__list-item_inspection">Талон техосмотра</div>
		</div>
		<button class="need__button order-call">Вызвать оценщика</button>
	</div>
</section>