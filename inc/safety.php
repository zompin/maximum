<section class="safety">
	<h1 class="safety__header">Почему это безопасно?</h1>
	<div class="safety__items">
		<div class="safety__item safety__item_scales">
			<p class="safety__p">Полная юридическая</p>
			<p class="safety__p">прозрачность сделок</p>
		</div>
		<div class="safety__item safety__item_mask">
			<p class="safety__p">Строгое соблюдение</p>
			<p class="safety__p">конфиденциальности</p>
		</div>
		<div class="safety__item safety__item_store">
			<p class="safety__p">Хранение автомобилей</p>
			<p class="safety__p">на специализированных парковках</p>
		</div>
	</div>
</section>