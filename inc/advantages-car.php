<section class="advantages">
	<h1 class="advantages__header">ПОЧЕМУ ИМЕННО МЫ?</h1>
	<div class="advantages__items">
		<div class="advantages__item advantages__item_reg">
			<p class="advantages__p">Быстрое оформление</p>
			<p class="advantages__p">кредита</p>
		</div>
		<div class="advantages__item advantages__item_docs">
			<p class="advantages__p">Минимальное количество</p>
			<p class="advantages__p">документов</p>
		</div>
		<div class="advantages__item advantages__item_bids">
			<p class="advantages__p">Выгодные ставки</p>
			<p class="advantages__p">по займам</p>
		</div>
		<div class="advantages__item advantages__item_safe">
			<p class="advantages__p">Гарантия сохранности</p>
			<p class="advantages__p">автомобиля</p>
		</div>
	</div>
	<button class="advantages__button order-call">Получить деньги!</button>
</section>