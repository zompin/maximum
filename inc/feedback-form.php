<div class="form__cont form__cont_feedback">
	<div class="form form_feedback">
		<div class="form__close-div"></div>
		<div class="form__inner form__inner_feedback">
			<button class="form__close"></button>
			<h1 class="form__header">Оставьте отзыв</h1>
			<div class="form__text">Нам очень важно ваше мнение для повышения качества обслуживания!</div>
			<div class="form__name-city">
				<label class="form__label form__label_name">
					<span class="form__label-caption">Имя*</span>
					<input type="text" class="form__input form__input_name form__input_name-feedback">
				</label>
				<label class="form__label form__label_city">
					<span class="form__label-caption">Город*</span>
					<input type="text" class="form__input form__input_phone form__input_city-feedback">
				</label>
			</div>
			<label class="form__label form__label_w">
				<span class="form__label-caption">E-mail* (не будет опубликован)</span>
				<input type="text" class="form__input form__input_phone form__input_email-feedback">
			</label>
			<div class="form__label form__label_comment">
				<span class="form__label-caption">Ваш отзыв (появится на сайте после проверки модератором)</span>
				<textarea class="form__textarea"></textarea>
			</div>
			<div class="form__captcha">
				<span class="form__captcha-text">Текст с картинки*</span>
				<input type="text" class="form__input form__input_captcha">
				<img src="<?php echo get_template_directory_uri(); ?>captcha/captcha.php" class="form__captcha-numbers">
			</div>
			<div class="form__text">Поля отмеченные * обязательны к заполнению.</div>
			<button class="form__button form__button_feedback">Отправить отзыв</button>
		</div>
	</div>
</div>