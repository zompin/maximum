<section class="advantages">
	<h1 class="advantages__header">Преимущества получения кредита под золото</h1>
	<div class="advantages__items">
		<div class="advantages__item advantages__item_bids">
			<p class="advantages__p">Выгодные ставки</p>
			<p class="advantages__p">по займам</p>
		</div>
		<div class="advantages__item advantages__item_docs">
			<p class="advantages__p">Минимальное количество</p>
			<p class="advantages__p">документов</p>
		</div>
		<div class="advantages__item advantages__item_pay">
			<p class="advantages__p">Мгновенная</p>
			<p class="advantages__p">оплата</p>
		</div>
		<div class="advantages__item advantages__item_calendar">
			<p class="advantages__p">Минимальный срок</p>
			<p class="advantages__p">кредитования</p>
		</div>
	</div>
	<button class="advantages__button order-call">Получить деньги!</button>
</section>