<section class="interesting">
	<div class="interesting__items">
		<div class="interesting__item interesting__item_text">
			<h1 class="interesting__header">Вас это заинтересует!</h1>
			<div class="interesting__list">
				<div class="interesting__list-item">Вы давно планируете открыть свой бизнес, но не знаете,  с чего начать, или боитесь «прогореть»?</div>
				<div class="interesting__list-item">У вас уже есть бизнес, но он не приносит желаемой прибыли?</div>
				<div class="interesting__list-item">Вы хотите иметь надежный источник пассивного дохода?</div>
				<div class="interesting__list-item">Вы хотите грамотно распорядиться своими сбережениями, и поэтому вам нужен долгосрочный, беспроигрышный и высокодоходный инвестиционный проект?</div>
			</div>
			<div class="interesting__text">Вы легко решите все эти задачи, приобретая франшизу ломбарда «МАКСИМУМ»!</div>
		</div>
		<div class="interesting__item interesting__item_image"></div>
	</div>
</section>