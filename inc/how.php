<section class="how">
	<h1 class="how__header">КАК МЫ РАБОТАЕМ?</h1>
	<div class="how__items">
		<div class="how__item">
			<div class="how__item-header">Оцениваем Ваш залог</div>
			<div class="how__item-text">
				Мы оцениваем залоговое имущество <br> 
				от 50 до 80% от рыночной стоимости <br> 
				в зависимости от частоты ваших обращений.
			</div>
		</div>
		<div class="how__item">
			<div class="how__item-header">Заключаем договор</div>
			<div class="how__item-text">
				Для заключения договора займа <br>
				вам нужен только документ <br>
				удостоверяющий личность!
			</div>
		</div>
		<div class="how__item">
			<div class="how__item-header">Выдаём деньги!</div>
			<div class="how__item-text">
				Вы получаете необходимые вам средства <br>
				на срок от 10 до 90 дней по низкой ставке <br>
				и без проволочек.
			</div>
		</div>
	</div>
</section>