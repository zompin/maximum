<section class="services">
	<h1 class="services__header">Услуги ломбарда</h1>
	<div class="services__items">
		<div class="services__item services__item_gold">
			<h2 class="services__item-header">
				<span class="services__money">Деньги под залог</span>
				<span class="services__type">Золота</span>
			</h2>
			<div class="services__percent">От 0,3% <br> в сутки!</div>
			<a href="<?php echo home_url('/zoloto.php'); ?>" class="services__link">Перейти</a>
		</div>
		<div class="services__item services__item_technic">
			<h2 class="services__item-header">
				<span class="services__money">Деньги под залог</span>
				<span class="services__type">Техники</span>
			</h2>
			<div class="services__percent">От 0,4% <br> в сутки!</div>
			<a href="<?php echo home_url('/tehnika.php'); ?>" class="services__link">Перейти</a>
		</div>
		<div class="services__item services__item_car">
			<h2 class="services__item-header">
				<span class="services__money">Деньги под залог</span>
				<span class="services__type">Авто</span>
			</h2>
			<div class="services__percent">С гарантией <br> безопасности!</div>
			<a href="<?php echo home_url('/avto.php'); ?>" class="services__link">Перейти</a>
		</div>
	</div>
</section>