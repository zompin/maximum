<section class="choose">
	<h1 class="choose__header">Почему выбирают франшизу ломбарда «максимум»?</h1>
	<div class="choose__items">
		<div class="choose__item choose__item_gear">
			<div class="choose__item-header">Полная автоматизация</div>
			<div class="choose__item-text">
				<div class="choose__p">С помощью специального ПО</div>
				<div class="choose__p">мы исключаем мошенничество</div>
				<div class="choose__p">со стороны сотрудников, а также</div>
				<div class="choose__p">автоматизируем работу ломбарда.</div>
			</div>
		</div>
		<div class="choose__item choose__item_bag">
			<div class="choose__item-header">Экономим ваши деньги</div>
			<div class="choose__item-text">
				<div class="choose__p">Дешевле, быстрей и удобнее</div>
				<div class="choose__p">приобрести нашу франшизу</div>
				<div class="choose__p">ломбарда, чем открыть</div>
				<div class="choose__p">его самостоятельно.</div>
			</div>
		</div>
		<div class="choose__item choose__item_brain">
			<div class="choose__item-header">Постоянное обновление</div>
			<div class="choose__item-text">
				<div class="choose__p">Мы постоянно развиваемся</div>
				<div class="choose__p">сами и делимся полученными</div>
				<div class="choose__p">знаниями и наработанным</div>
				<div class="choose__p">опытом с нашими франчайзи.</div>
			</div>
		</div>
		<div class="choose__item choose__item_shield">
			<div class="choose__item-header">Гарантия безопасности</div>
			<div class="choose__item-text">
				<div class="choose__p">Мы позаботились о том,</div>
				<div class="choose__p">чтобы ваши риски были</div>
				<div class="choose__p">минимальными как юридически,</div>
				<div class="choose__p">так и финансово.</div>
			</div>
		</div>
	</div>
</section>