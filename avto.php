<?php
	/*
		Template name: Авто
	*/
		
	$page_title = 'Авто';
	include 'header.php';
?>
<div class="slide slide__car">
	<div class="slide__box">
		Быстрые займы
		<br>
		Под залог автомобиля
	</div>
	<div class="slide__text">
		<div class="slide__text-take">Возьми деньги</div>
		<div class="slide__text-without">Без процентов!</div>
	</div>
	<button class="slide__button order-call">Получить деньги!</button>
</div>
<?php
	get_template_part('inc/advantages-car');
	get_template_part('inc/need-car');
	get_template_part('inc/safety');
	get_template_part('inc/rate-avto');
	get_template_part('inc/feedback-car');
	include 'footer.php';
?>