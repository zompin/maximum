<?php
	/*
		Template name: Техника
	*/
		
	$page_title = 'Золото';
	include 'header.php';
?>
<div class="slide slide__tech">
	<div class="slide__box">
		Быстрые займы
		<br>
		Под залог техники
	</div>
	<div class="slide__text">
		<div class="slide__text-take">Возьми деньги</div>
		<div class="slide__text-without">Без процентов!</div>
	</div>
	<button class="slide__button order-call">Получить деньги!</button>
</div>
<?php
	get_template_part('inc/wares-tech');
	get_template_part('inc/advantages-tech');
	get_template_part('inc/need-tech');
	get_template_part('inc/scheme-tech');
	get_template_part('inc/rate-tech');
	get_template_part('inc/feedback-tech');
	include 'footer.php';
?>